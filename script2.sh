#!/bin/bash
ARG1=$1 #Przypisanie zmiennej do pierwszego podanego argumentu
ARG2=$2 #Przypisanie zmiennej do drugiego podanego argumentu
ARG3=$3

if [ "$ARG1" = "lower" ] && [ "$ARG2" = "upper" ] && [ -z "$ARG3" ]; then #sprawdza czy pierwszy argument rowna sie lower, "i" drugi argument upper, oraz czy trzeci jest pusty

    	find * -maxdepth 1 -execdir rename 'y/a-z/A-Z/' {} \; #find wyszkuje wszystkie pliki w katalogu w ktorym sie znajdue dzieki maxdepth 1
      
    	echo "Zmieniono nazwy wszystkich plikow na duze litety"  #wypisz wszystko z nawisow

elif [ "$ARG1" = "upper" ] && [ "$ARG2" = "lower" ] && [ -z "$ARG3" ]; then

	find * -maxdepth 1 -execdir rename 'y/A-Z/a-z/' {} \; #zmienia litery w nazwie plikow w katalogu na male litery za pomoca rename, execdir pokazuje informacje. Find przeszukuje kazdy plik

   	echo "Zmieniono nazwy wszystkich plikow na male litety"  
       
elif [ "$ARG1" = "lower" ] && [ "$ARG2" = "upper" ]; then

        find -maxdepth 1 -name $ARG3 -execdir rename 'y/a-z/A-Z/' {} \; #podobnie jak wyzej, jedyne co musialem dodac to -name i wskazac parametr trzeci. Ktory mowi ktorego pliku ma szukac find. W danym katalogu dzieki maxdepth, execdir wskazuje inforamcje, natomaist rename zmienia wielkosc liter 

        echo "Zmieniono nazwe pliku '$ARG3' na duze litety"  
#funkcje praktycznie sie powtarzaja. Mozna bylo uzyc rowniez switcha jednak dobrze mi się pracowalo z if

elif [ "$ARG1" = "upper" ] && [ "$ARG2" = "lower" ]; then

	find -maxdepth 1 -name $ARG3 -execdir rename 'y/A-Z/a-z/' {} \;

        echo "Zmieniono nazwe pliku '$ARG3' na male litety" 

elif [ "$ARG1" = "-h" ] || [ "$ARG1" = "--help" ]; then    

	        printf  "\n\n---------------POMOC------------------------- \n"
        printf "\nSkrypt zmieniajacy wielkosc liter nazw plikow znajdujacych sie w biezacym katalogu.\nSposob zmiany musi zostac podany w parametrze, gdyz jezeli nie zostanie podany, \n skrypt przyjmuje wszystkie pliki w bierzacy katalogu.\n"
        printf "\nPRZYKLAD\n ./script2.sh lower upper -> zmienia wszystkie  nazwy plikow na wielkie litery (bierzacy katalog)\n"
	printf "./script2.sh upper lower -> zmienia wszystkie nazwy plikow na male litery (bierzacy katalog)\n"
	printf "./script2.sh lower upper [NAZWAPLIKU] zmienia nazwe podanego pliku na duze litery.\n Analogicznie skrypt zadziala dla parametrow upper lower [NAZWAPLIKU]"
        printf "\n\nUZYCIE\n ./script2.sh [lower/upper] [upper/lower] [nazwapliku/braknazwy] \n\n"

        

else
        echo "Twoje polecenie jest bledne, sprobuj uzyc -h lub --help , aby uzyskac pomoc"
fi


