#!/bin/bash

 X=1; #przypiasnie liczby do zmiennej

if [ -z "$1" ]; then #sprawdzenie czy argument o indeksie pierwszym jest pusty.
	for filename in *; do  #petla dla kazdego pliku w obecnym katalogu

		mv "${filename}" "${X}"_"${filename}"; #zmienia obecna nazwe pliku dodajac na poczatku LICZBAX_POPRZEDNIA NAZWA
		#mv sluzy rowniez do przenoszenia plikow, jednak rowniez mozna jej uzywac do zmiany nazwy
		X=$((X+1)); #inkrementuje zmienna X, dzieki czemu w nastepej petli X bedzie o jeden wiekszy

	done #zakonczenie petli for 

 elif [ "$1" = "-h" ] || [ "$1" = "--help" ]; then #jesli argument o indeksie 1 bedzie rowny -h lub --help, wykonas polecenie po 'then'
 
 	printf	"\n\n---------------POMOC------------------------- "
	printf "\nSkrypt numerjacy wszystkie pliki w biezacym katalogu.\nWynik dzialania skryptu to zmiana nazwy plikow, dodajac na poczatku numer + podkreślenie.\n"
	printf "PRZYKLAD\n 1_przykadowyplikpierwszy\n 2_przykladowyplikdrugi\n 3_przykladowypliktrzeci"
	printf "\n\nUZYCIE\n ./script3.sh [ENTER] \n\n"
	 

fi #koniec instrukcji warunkowej 

