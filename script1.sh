#!/bin/bash

INPUTWORD=$1 #Przypisanie zmiennej do pierwszego podanego argumentu
WORDTOREPLACE=$2 #Przypisanie zmiennej do drugiego podanego argumentu


if [ "$INPUTWORD" = "-h" ] || [ "$INPUTWORD" = "--help" ] ; then

        printf  "\n\n---------------POMOC------------------------- "
        printf "\n\nSkrypt zmieniajacy wskazana dana w pierwszym parametrze,\n na inna (podana w drugim parametrze) Wynik dzialania skryptu \n obejmuje wszystkie pliki w obecnym katalogu, oraz \n wszystkie pliki znajdujcce sie w podkatalogach.\n"
        printf "\nPRZYKLAD\n ./script1.sh Kuba Marek\n We wszystkich plikach w danym katalogu oraz podkatalogow\n Slowo Kuba zostanie zamienione na Marek"
        printf "\n\nUZYCIE\n ./script1.sh [PARAMETR1] [PARAMETR2] \n\n"

else
	find ./ -type f -exec sed -i 's/'$INPUTWORD'/'$WORDTOREPLACE'/g' {} \;
	echo " Zmieniono we wszystkich plikach slowo '$INPUTWORD' na slowo '$WORDTOREPLACE' "
fi
